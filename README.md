# Upay

## Setting up

  * Install [Elixir 1.5](http://elixir-lang.org/install.html)
  * Install [PostgreSQL](https://www.postgresql.org/)
  * Go to the project directory: `cd upay`
  * Fetch all dependencies: `mix deps.get`
  * Initialize event store: `mix upay.setup`
  * Start server: `mix phx.server`


## To reset db

  * `mix upay.reset`


## Description

This is elixir umbrella application based on [phoenix framework (v1.3)](http://phoenixframework.org/)
and [commanded](https://github.com/commanded/commanded) library.

It exposes 3 json endpoints

  * `POST /api/payments` with example payload `{"txhash": "0x0e22bcf4394c323991668ee4f304048b10ef915490708b4e158c98aff8ce0a34"}`
      which receives payment.
  * `GET /api/payments` which list all payments
  * `GET /api/payments/0x0e22bcf4394c323991668ee4f304048b10ef915490708b4e158c98aff8ce0a34` which shows single payment information.


## Architecture

At the moment because of simplicity of requirements Upay uses `ets` as underlying store,
although changing it to any other persistence store like `postgres` should be
fairly easy.

This is first time I've developed application based on CQRS/ES rules and I've
learned a lot, but I'm definitely still lacking practice and good guides how to
organize code.

There are 2 commands:

  * `ReceivePayment` which initializes whole payment process. This is blocking operation which checks if transaction exists at all. Blocking is used to get eventual errors back to user.
  * `CheckPayment` for checking periodically [Etherscan](https://etherscan.io/) for transactions statuses.

There are 2 events:

  * `PaymentReceived` which is used to insert register transaction in the system.
  * `PaymentComplete` for finishing payment process.


## Testing

At this moment there are no tests, because this was basically architecture
learning experience with a lots of moving parts, sometimes glued together with
surprising effects.

I'd would definitely export scraping Etherscan part to it's own module and add
mock for testing.

  * There are lots of unit tests needed.
  * There should be contract tests between `Upay` and `UpayWeb`.
  * Integration tests needed for `Upay` to see if relevant information lands in databases.
  * End-to-end tests to check if it works. Even `Curl` based.

###How many tests?
Lots = Unit > Contract > Integration > End-to-end = Few
