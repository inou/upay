use Mix.Config

# Configure your database
config :eventstore, EventStore.Storage,
  serializer: Commanded.Serialization.JsonSerializer,
  username: "postgres",
  password: "postgres",
  database: "upay_eventstore_test",
  hostname: "localhost",
  pool_size: 10
