use Mix.Config

# Configure your database
config :eventstore, EventStore.Storage,
  serializer: Commanded.Serialization.JsonSerializer,
  username: "postgres",
  password: "postgres",
  database: "upay_eventstore_dev",
  hostname: "localhost",
  pool_size: 10

config :upay,
  call_interval: 20

