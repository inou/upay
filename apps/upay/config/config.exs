use Mix.Config

config :upay, ecto_repos: [Upay.Repo]

config :commanded,
  event_store_adapter: Commanded.EventStore.Adapters.EventStore

import_config "#{Mix.env}.exs"
