defmodule Upay.Paywall do
  @moduledoc """
  The boundary for the Paywall.
  """

  alias Upay.Paywall.Router
  alias Upay.Paywall.Query
  alias Upay.Paywall.Commands.ReceivePayment

  defdelegate get_payments(), to: Query
  defdelegate get_payment_by_txhash(txhash), to: Query

  def receive_payment(params) do
    receive_payment = ReceivePayment.new(params)
    with :ok <- Router.dispatch(receive_payment) do
      :ok
    else
      reply -> reply
    end
  end
end
