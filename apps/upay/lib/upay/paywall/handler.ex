defmodule Upay.Paywall.PaymentHandler do
  use Commanded.Event.Handler, name: __MODULE__

  @call_interval Application.get_env(:upay, :call_interval, 30)

  alias Upay.Paywall.Events.{
    PaymentReceived,
    PaymentCompleted
  }
  alias Upay.Paywall.Commands.CheckPayment
  alias Upay.Paywall.Scheduler

  def init do
    :ets.new(Upay.Payments, [:named_table])
    :ok
  end

  def handle(%PaymentReceived{txhash: txhash}, _metadata) do
    true = insert(txhash, false)
    :ok = Scheduler.schedule(@call_interval, %CheckPayment{txhash: txhash})
    :ok
  end

  def handle(%PaymentCompleted{txhash: txhash}, _metadata) do
    insert(txhash, true)
    :ok
  end

  defp insert(key, value) do
    true = :ets.insert(Upay.Payments, {key, value})
  end
end
