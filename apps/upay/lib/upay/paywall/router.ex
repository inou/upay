defmodule Upay.Paywall.Router do
  use Commanded.Commands.Router

  alias Upay.Paywall.Commands.{
    ReceivePayment,
    CheckPayment
  }
  alias Upay.Paywall.Aggregates.Payment
  alias Upay.Middleware.Validate

  middleware Validate

  identify Payment, by: :txhash

  dispatch [ReceivePayment,CheckPayment],
    to: Payment,
    lifespan: Payment
end
