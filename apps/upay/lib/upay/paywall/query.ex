defmodule Upay.Paywall.Query do

  alias Upay.Paywall.Projections.Payment

  def get_payments do
    Upay.Payments
    |> :ets.tab2list()
    |> Enum.map(&to_params/1)
    |> Enum.map(&Payment.new/1)
  end

  def get_payment_by_txhash(txhash) do
    case lookup(txhash) do
      {:ok, params} ->
        {:ok, Payment.new(params)}
      {:error, :not_found} = error ->
        error
    end
  end

  defp lookup(key) do
    case :ets.lookup(Upay.Payments, key) do
      [{^key, _value} = pair] -> {:ok, to_params(pair)}
      [] -> {:error, :not_found}
    end
  end

  def to_params({key, value}) do
    %{txhash: key, completed: value}
  end
end
