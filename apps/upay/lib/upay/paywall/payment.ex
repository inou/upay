defmodule Upay.Paywall.Aggregates.Payment do
  @behaviour Commanded.Aggregates.AggregateLifespan

  defstruct [:txhash, :confirmed, :blocks]

  @blocks_to_confirm Application.get_env(:upay, :blocks_to_confirm, 2)

  require Logger

  alias Upay.Paywall.Aggregates.Payment
  alias Upay.Paywall.Commands.{
    ReceivePayment,
    CheckPayment
  }
  alias Upay.Paywall.Events.{
    PaymentReceived,
    PaymentCompleted
  }

  # public commmand API

  def execute(%Payment{txhash: nil}, %ReceivePayment{txhash: txhash}) do
    fetch_blocks_num(txhash)
  end

  # payment already registered
  def execute(%Payment{}, %ReceivePayment{}) do
    {:error, :already_registered}
  end

  def execute(%Payment{}, %CheckPayment{txhash: txhash}) do
    fetch_blocks_num(txhash)
  end

  # state mutators

  def apply(%Payment{} = payment, %PaymentReceived{txhash: txhash, blocks: blocks}) do
    %Payment{payment |
      txhash: txhash,
      blocks: blocks,
      confirmed: false
    }
  end

  def apply(%Payment{} = payment, %PaymentCompleted{txhash: txhash}) do
    %Payment{payment |
      txhash: txhash,
      confirmed: true
    }
  end

  # lifespan control

  def after_command(_), do: :infinity
  def after_event(%PaymentCompleted{}), do: :hibernate
  def after_event(_), do: :infinity

  # helpers

  defp fetch_blocks_num(txhash) do
    case check_existence(txhash) do
      {:error, :not_found} = error -> error
      {:ok, num} ->
        if num < @blocks_to_confirm do
          %PaymentReceived{txhash: txhash, blocks: num}
        else
          %PaymentCompleted{txhash: txhash}
        end
    end

  end

  defp check_existence(txhash) do
    case HTTPoison.get("https://etherscan.io/tx/" <> txhash) do
      {:ok, response} ->
        case Regex.run(~r/([0-9]+) block confirmation/,
                       response.body,
                       capture: :all_but_first) do
          nil -> {:error, :not_found}
          [num] -> {:ok, String.to_integer(num)}
        end
      # FIXME: simplification - HTTPoison returns different errors which
      # should be handled gracefully
      {:error, _} -> {:error, :not_found}
    end
  end
end
