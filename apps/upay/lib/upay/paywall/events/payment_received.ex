defmodule Upay.Paywall.Events.PaymentReceived do
  defstruct [:txhash, :blocks]
end
