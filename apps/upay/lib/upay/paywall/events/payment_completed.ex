defmodule Upay.Paywall.Events.PaymentCompleted do
  defstruct [:txhash]
end
