defmodule Upay.Paywall.Projections.Payment do
  defstruct [:txhash, :completed]

  use ExConstructor
end
