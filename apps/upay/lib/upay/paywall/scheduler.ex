defmodule Upay.Paywall.Scheduler do
  use GenServer

  alias Upay.Paywall.Router

  def start_link do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def schedule(in_sec, what) do
    GenServer.cast(__MODULE__, {:schedule, in_sec, what})
  end

  def handle_cast({:schedule, in_sec, what}, state) do
    Process.send_after(self(), {:when, what}, in_sec * 1000)
    {:noreply, state}
  end

  def handle_info({:when, what}, state) do
    :ok = Router.dispatch(what)
    {:noreply, state}
  end
end
