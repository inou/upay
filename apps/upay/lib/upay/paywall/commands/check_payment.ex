defmodule Upay.Paywall.Commands.CheckPayment do
  defstruct [:txhash]
  use ExConstructor
  use Vex.Struct

  validates :txhash,
    presence: true,
    format: ~r/^0x[a-fA-F0-9]+$/,
    length: 66
end
