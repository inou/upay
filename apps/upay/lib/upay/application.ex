defmodule Upay.Application do
  @moduledoc """
  The Upay Application Service.

  The upay system business domain lives in this application.

  Exposes API to clients such as the `UpayWeb` application
  for use in channels, controllers, and elsewhere.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Supervisor.start_link([
      worker(Upay.Paywall.PaymentHandler, []),
      worker(Upay.Paywall.Scheduler, [])
    ], strategy: :one_for_one, name: Upay.Supervisor)
  end
end
