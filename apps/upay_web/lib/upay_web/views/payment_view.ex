defmodule UpayWeb.PaymentView do
  use UpayWeb, :view

  def render("index.json", %{payments: payments}) do
    render_many(payments, __MODULE__, "payment.json")
  end

  def render("show.json", %{payment: payment}) do
    render_one(payment, __MODULE__, "payment.json")
  end

  def render("payment.json", %{payment: payment}) do
    %{txhash: payment.txhash,
      completed: payment.completed}
  end

end
