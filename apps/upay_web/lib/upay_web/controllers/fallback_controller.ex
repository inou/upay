defmodule UpayWeb.FallbackController do
  use UpayWeb, :controller

  def call(conn, {:error, :validation_failure, errors}) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(UpayWeb.ErrorView, "error.json", errors: errors)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(UpayWeb.ErrorView, :"404")
  end

  def call(conn, {:error, :already_registered}) do
    conn
    |> send_resp(:no_content, "")
  end
end
