defmodule UpayWeb.PaymentController do
  use UpayWeb, :controller

  alias Upay.Paywall

  action_fallback UpayWeb.FallbackController

  def index(conn, _params) do
    payments = Paywall.get_payments()
    render(conn, "index.json", payments: payments)
  end

  def show(conn, %{"id" => txhash}) do
    with {:ok, payment} <- Paywall.get_payment_by_txhash(txhash) do
      render(conn, "show.json", payment: payment)
    end
  end

  def create(conn, params) do
    with :ok <- Paywall.receive_payment(params) do
      conn
      |> send_resp(:created, "")
    end
  end
end
