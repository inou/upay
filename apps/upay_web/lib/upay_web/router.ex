defmodule UpayWeb.Router do
  use UpayWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", UpayWeb do
    pipe_through :api

    resources "/payments", PaymentController, only: [:index, :show, :create]
  end
end
